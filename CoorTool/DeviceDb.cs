﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;

namespace DeviceDB
{
    class deviceDb
    {
        public struct device_str
        {
            public string Type { get; set; }
            public byte EndPoint { get; set; }
            public Int16 Nwk { get; set; }
            public string Mac { get; set; }
            public byte State { get; set; }

            public device_str(string Type_t, byte EndPoint_t, Int16 Nwk_t, string Mac_t, byte State_t)
            {
                this.Type = Type_t;
                this.EndPoint = EndPoint_t;
                this.Nwk = Nwk_t;
                this.Mac = Mac_t;
                this.State = State_t;
            }

            public object[] all
            {
                get { return new object[] { string.Join(",", Type), EndPoint, Nwk, string.Join(",", Mac), State }; }
                set
                {
                    Type = (string)(value[0]);
                    EndPoint = (byte)value[1];
                    Nwk = (Int16)value[2];
                    Mac = (string)(value[3]);
                    State = (byte)value[4];
                }
            }
        }

        public DataTable deviceTable = new DataTable();
        public List<device_str> listDevice = new List<device_str>();

        public struct device_db_str
        {
            public Int32 Id;
            public device_str dev_info;
            public device_db_str(Int16 id_t, device_str dev_info_t)
            {
                this.Id = id_t;
                this.dev_info = dev_info_t;
            }
            public object[] all
            {
                get { return new object[] { Id, string.Join(",", dev_info.Type), dev_info.EndPoint, dev_info.Nwk, string.Join(",", dev_info.Mac), dev_info.State }; }
                set {
                    Id = (Int32)value[0];
                    dev_info.Type = (string)(value[1]);
                    dev_info.EndPoint = (byte)value[2];
                    dev_info.Nwk = (Int16)value[3];
                    dev_info.Mac = (string)(value[4]);
                    dev_info.State = (byte)value[5];
                }
            }
        }
        public DataGridView deviceDataGridView;


        public device_db_str test_device = new device_db_str { Id = 0, all = { } };

        public void Init(DataGridView dataGrid)
        {


            this.deviceDataGridView = dataGrid;

            this.deviceTable.Columns.Add("DeviceId", typeof(Int32));
            this.deviceTable.Columns.Add("Type", typeof(string));
            this.deviceTable.Columns.Add("EndPoint", typeof(byte));
            this.deviceTable.Columns.Add("Nwk", typeof(Int16));
            this.deviceTable.Columns.Add("Mac", typeof(string));
            this.deviceTable.Columns.Add("State", typeof(byte));

            deviceDataGridView.DataSource = this.deviceTable;

            this.Add(test_device);


        }

        public void Add(device_db_str dev)
        {
            this.deviceTable.Rows.Add(dev.all);
        }
        public void Add(device_db_str[] dev)
        {
            foreach (device_db_str item in dev) {
                this.deviceTable.Rows.Add(item.all);
            }
        }
        public device_str DeviceRowToDeviceDb(DataRow devRow)
        {
            device_str dev = new device_str();
            dev.Type = devRow["Type"].ToString();
            dev.EndPoint = Convert.ToByte(devRow["EndPoint"]);
            dev.Nwk = Convert.ToInt16(devRow["Nwk"]);
            dev.Mac = devRow["Mac"].ToString();
            dev.State = Convert.ToByte(devRow["State"]);

            return dev;
        }
        public device_str[] Read(Int32 IdOrNwk)
        {

            DataView dataView = new DataView();
            try
            {
                dataView.RowFilter = String.Format("DeviceId = '{0}'", IdOrNwk.ToString());

                DataRow[] devRow = this.deviceTable.Select(dataView.RowFilter);
                device_str[] dev = new device_str[devRow.Length];
                for (int i = 0; i < devRow.Length; i++)
                {
                    dev[i] = DeviceRowToDeviceDb(devRow[i]);
                }
                return dev;
            }
            catch { }
            try
            {
                dataView.RowFilter = String.Format("Nwk = '{0}'", IdOrNwk.ToString());

                DataRow[] devRow = this.deviceTable.Select(dataView.RowFilter);
                device_str[] dev = new device_str[devRow.Length];
                for (int i = 0; i < devRow.Length; i++)
                {
                    dev[i] = DeviceRowToDeviceDb(devRow[i]);
                }
                return dev;
            }
            catch { }
            return null;
        }

        public device_str[] Read(String MacOrType)
        {

            DataView dataView = new DataView();

            try
            {
                dataView.RowFilter = String.Format("Mac = '{0}'", MacOrType);

                DataRow[] devRow = this.deviceTable.Select(dataView.RowFilter);
                device_str[] dev = new device_str[devRow.Length];
                for (int i = 0; i < devRow.Length; i++)
                {
                    dev[i] = DeviceRowToDeviceDb(devRow[i]);
                }
                return dev;
            }
            catch { }
            try
            {
                dataView.RowFilter = String.Format("Type = '{0}'", MacOrType);

                DataRow[] devRow = this.deviceTable.Select(dataView.RowFilter);
                device_str[] dev = new device_str[devRow.Length];
                for (int i = 0; i < devRow.Length; i++)
                {
                    dev[i] = DeviceRowToDeviceDb(devRow[i]);
                }
                return dev;
            }
            catch { }
            return null;
        }
        public void Delete(Int32 IdOrNwk)
        {
            DataView dataView = new DataView();
            try
            {
                dataView.RowFilter = String.Format("DeviceId = '{0}'", IdOrNwk.ToString());
                DataRow[] deviceRow = this.deviceTable.Select(dataView.RowFilter);
                for (int i = 0; i < deviceRow.Length; i++)
                {
                    deviceRow[i].Delete();
                }
            }
            catch { }
            try
            {
                dataView.RowFilter = String.Format("Nwk = '{0}'", IdOrNwk.ToString());
                DataRow[] deviceRow = this.deviceTable.Select(dataView.RowFilter);
                for (int i = 0; i < deviceRow.Length; i++)
                {
                    deviceRow[i].Delete();
                }
            }
            catch { }
        }
    }
}
