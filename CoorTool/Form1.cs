﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;
using System;
using System.Timers;
using System.Runtime.InteropServices;
using DeviceDB;
using Serial;
using Message;
using System.Collections;

namespace CoorTool
{
    public partial class Form1 : Form
    {


        deviceDb user_deviceDb = new deviceDb();
        message user_message = new message();


        public Form1()
        {
            InitializeComponent();
        }

        private void serialBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
        private void button1_Click(object sender, EventArgs e)
        {
            user_message.user_serial.comPort.open();
        }
        private void buttonClose_Click(object sender, EventArgs e)
        {
            user_message.user_serial.comPort.close();
        }

        private void serialDebugBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            user_message.OpenNwk();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            user_message.CloseNwk();
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        { 
            user_deviceDb.Init(dataGridView1);
            user_message.user_serial.Init(new comPort.ComPortLayout { 
                _buttonClose = buttonClose , 
                _buttonOpen = buttonOpen,
                _cBoxSelect = comboBox1,
                _serial = serialPort1,
                _serialOut = serialDebugBox,
                _form = this
            });
            user_message.InitMessageCallback();

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }
    }
}
