﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DeviceDB;
using Serial;

namespace Message
{
    class message
    {
        public serial user_serial = new serial();
        public comPort user_comPort = new comPort();
        public serial.Cmd_t cmd = new serial.Cmd_t();
        public deviceDb user_deviceDb = new deviceDb();


        public struct FrameUartErrorRev_ft
        {
            public byte CmdID;
            public byte ErorType;
            public byte ErrorCode;
            public byte seqNumber;
        }
        public struct FrameUartErrorTran_ft
        {
            public byte CmdID;
            public byte ErrorCode;
            public byte seqNumber;
        }
        public struct PermitJoinReq_ft
        {
            public byte CmdID;
            public byte Time;
        }

        public int MAC_LENGTH = 8;

        enum Cmd_ID_Recv
        {
            ZB_UART_ERRR = 0x00,
            ZB_NWK_INFO = 0x01,
            ZB_PERMIT_STATE = 0x05,
            ZB_ZCL_CLUSTER_RP = 0x06,
            ZB_ZCL_GLOBAL_RP = 0x07,
            ZB_ZDO_RP        = 0x08,
        };
        enum Cmd_ID_Send
        {
            HOST_ERROR = 0x80,
            HOST_FORM_NWK = 0x81,
            HOST_GET_NWK_INFO = 0x82,
            HOST_PERMIT_JOIN = 0x83,
            HOST_GET_PERMIT_STATE = 0x85,
            HOST_ZCL_CLUSTER_CMD    =   0x86,
            HOST_ZCL_GLOBAL_CMD = 0x87,
            HOST_ZDO_CMD = 0x88,
            HOST_RESET_ZB = 0x8E,
        };
        public enum Zdo_Cmd_e
        {
            END_DEVICE_ANNOUNCE       =   0x0013
        }

        public void InitMessageCallback()
        {
            user_comPort.init_rxcb(Parse_common);
        }
        void Parse_common (byte[] packet)
        {
            int index = 0;
            deviceDb.device_str dev = new deviceDb.device_str();
            byte Cmd_ID = packet[index++];
            switch (Cmd_ID){
                case (byte)Cmd_ID_Recv.ZB_ZCL_CLUSTER_RP:
                    dev.Nwk = (Int16)((packet[index++] << 8) + packet[index++]);
                    dev.EndPoint = packet[index++];
                    // search device id
                    // parse with device id and data

                    break;
                case (byte)Cmd_ID_Recv.ZB_ZCL_GLOBAL_RP:
                
                    break;
                case (byte)Cmd_ID_Recv.ZB_ZDO_RP:
                    dev.Nwk = (Int16)((packet[index++] << 8) + packet[index++]);
                    ParseZDO(dev, packet.Skip(index).Take(packet.Length - index).ToArray());
                    break;
                default:
                    break;
            }
        }
        public void ParseZDO(deviceDb.device_str dev, byte[] data)
        {
            int index = 0;
            deviceDb.device_str[] read_dev = user_deviceDb.Read(dev.Nwk);
            Int16 Zdo_command_id = (Int16)(data[index++] << 8 | data[index++]);
            switch (Zdo_command_id)
            {
                case (Int16)Zdo_Cmd_e.END_DEVICE_ANNOUNCE:
                    byte data_length = data[index++];
                    byte transaction_seq_num = data[index++];
                    Int16 nodeId = (Int16)(data[index++] | data[index++] << 8);
                    string eui64 = BitConverter.ToString((data.Skip(index).Take(MAC_LENGTH).ToArray())).Replace("-", "");

                    Console.WriteLine("nodeid" + Convert.ToBase64String(BitConverter.GetBytes(nodeId)));
                    Console.WriteLine("eui64" + eui64);
                    if(read_dev == null)
                    {
 //                       read_dev[0] = new deviceDb.device_str { EndPoint = 1, Mac = eui64, Nwk = nodeId, State = 0, Type  = "cong tac 2"};
 //                       read_dev[1] = new deviceDb.device_str { EndPoint = 2, Mac = eui64, Nwk = nodeId, State = 0, Type = "cong tac 2" };
//                        deviceDb.device_db_str[] read_dev_db = new deviceDb.device_db_str[2];
//                        read_dev_db[0] = new deviceDb.device_db_str { Id = 0, dev_info = read_dev[0] };
//                        read_dev_db[1] = new deviceDb.device_db_str { Id = 1, dev_info = read_dev[1] };
//                        user_deviceDb.Add(read_dev_db);
                    }


                    break;

            }

        }
        public void Parse_ZCL_global(deviceDb.device_str dev, byte[] data)
        {

        }
        public void Parse_ZCL_Cluster(deviceDb.device_str dev, byte[] data)
        {

        }
        public void ZCL_global_cmd(deviceDb.device_str dev, byte[] data)
        {

        }
        public void ZCL_cluster_cmd(deviceDb.device_str dev, byte[] data)
        {

        }
        public void ZDO_cmd(deviceDb.device_str dev, byte[] data)
        {

        }

        public void OpenNwk()
        {

            PermitJoinReq_ft permitJoin_cmd;
            cmd.seqNumber = user_serial.globalSeqNumber++;
            cmd.length = 4;
            permitJoin_cmd.CmdID = 0x83;
            permitJoin_cmd.Time = 0xFF;
            cmd.cmdData = user_serial.StructureToByteArray(permitJoin_cmd);
            user_serial.SendCmd(cmd);

        }
        public void CloseNwk()
        {
            PermitJoinReq_ft permitJoin_cmd;
            cmd.seqNumber = user_serial.globalSeqNumber++;
            cmd.cmdId = 0x83;
            cmd.length = 4;
            permitJoin_cmd.CmdID = 0x83;
            permitJoin_cmd.Time = 0x00;
            cmd.cmdData = user_serial.StructureToByteArray(permitJoin_cmd);
            user_serial.SendCmd(cmd);

        }
    }
}
