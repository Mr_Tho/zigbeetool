﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using System.Collections;

namespace Serial
{
    public class comPort
    {
        private int Recv_State;
        enum Recv_State_enum
        {
            StartCheckHighByte = 0,
            StartCheckLowByte = 1,
            Recv_lengh = 2,
            Recv_seq = 3,
            Recv_data = 4,
            Recv_checkXor = 5
        }
        private int MAX_RECV_CMD_LENGTH = 128;
        private struct cmd_t {
            public byte[] data;
            public byte seq;
            public byte length_data;
            public byte count;
        }

        private cmd_t RecvCmd = new cmd_t {data = new byte[128] };

        public delegate void rx_callback(byte[] data);
        private static rx_callback rx_cb; 

        public void init_rxcb(rx_callback cb)
        {
            if(cb != null)
            {
                rx_cb = cb;
            }
        }

        public struct ComPortLayout
        {
            public ComboBox _cBoxSelect;
            public Button _buttonOpen;
            public Button _buttonClose;
            public SerialPort _serial;
            public RichTextBox _serialOut;
            public Form _form;
        };
        public ComPortLayout comPortLayout;
        private byte[] StartByte = { 0x4C, 0x4D };

        delegate void SetTextCallback(Form f, RichTextBox ctrl, string text);

        public static void SetText(Form form, RichTextBox ctrl, string text)
        {

            if (ctrl.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                form.Invoke(d, new object[] { form, ctrl, text });
            }
            else
            {
                ctrl.AppendText(text);
            }
        }

        public void Init(ComPortLayout cp_layout)
        {
            comPortLayout._cBoxSelect = cp_layout._cBoxSelect;
            comPortLayout._buttonOpen = cp_layout._buttonOpen;
            comPortLayout._buttonClose = cp_layout._buttonClose;
            comPortLayout._serial = cp_layout._serial;
            comPortLayout._serialOut = cp_layout._serialOut;
            comPortLayout._form = cp_layout._form;

            string[] ports = SerialPort.GetPortNames();
            comPortLayout._cBoxSelect.Items.AddRange(ports);
            comPortLayout._serial.DataReceived += new SerialDataReceivedEventHandler(serialDataReceiverHandle);

        }
        public void serialDataReceiverHandle(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;
            byte[] b_data = new byte[sp.BytesToRead];
            sp.Read(b_data, 0, sp.BytesToRead);

            debugDataPrintLn("<< " + BitConverter.ToString(b_data).Replace("-", " ") + "\n\r");

            for(int i= 0; i< b_data.Length; i++)
            {
                switch (Recv_State)
                {
                    case (byte)Recv_State_enum.StartCheckHighByte:
                        if(b_data[i] == StartByte[0])
                        {
                            Recv_State = (byte)Recv_State_enum.StartCheckLowByte;
                        }
                        else
                        {
                            goto end;
                        }
                        break;
                    case (byte)Recv_State_enum.StartCheckLowByte:
                        if(b_data[i] == StartByte[1])
                        {
                            Recv_State = (byte)Recv_State_enum.Recv_lengh;
                        }
                        else
                        {
                            goto end;
                        }
                        break;
                    case ((byte)Recv_State_enum.Recv_lengh):
                        if (b_data[i] > MAX_RECV_CMD_LENGTH)
                        {
                            goto end;
                        }
                        else
                        {
                            RecvCmd.length_data = b_data[i];
                            Recv_State = (byte)Recv_State_enum.Recv_seq;
                        }
                        break;
                    case ((byte)Recv_State_enum.Recv_seq):
                        RecvCmd.seq = b_data[i];
                        Recv_State = (byte)Recv_State_enum.Recv_data;
                        break;

                    case ((byte)Recv_State_enum.Recv_data):
                        RecvCmd.data[RecvCmd.count] = b_data[i];
                        RecvCmd.count++;

                        if(RecvCmd.count == RecvCmd.length_data - 2)
                        {
                            Recv_State = (byte)Recv_State_enum.Recv_checkXor;
                            RecvCmd.count = 0;
                        }
                        break;
                    case ((byte)Recv_State_enum.Recv_checkXor):
                        byte Check_Xor = RecvCmd.data[0];
                        for(int j = 0; j < RecvCmd.length_data - 3; j++)
                        {
                            Check_Xor ^= RecvCmd.data[j+1];
                        }
                        Console.WriteLine("Recv cmd");
                        rx_cb(RecvCmd.data);

                        end:
                        RecvCmd.length_data = 0;
                        RecvCmd.count = 0;
                        Array.Clear(RecvCmd.data, 0, RecvCmd.length_data);
                        Recv_State = (byte)Recv_State_enum.StartCheckHighByte;
                        break;

                }
            }
        }

        public void open()
        {
            try
            {
                comPortLayout._serial.PortName = comPortLayout._cBoxSelect.Text;
                comPortLayout._serial.Open();
                if (comPortLayout._serial.IsOpen)
                {
                    comPortLayout._buttonOpen.Enabled = false;
                    comPortLayout._buttonClose.Enabled = true;
                    debugDataPrintLn("Open Port OK");
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void close()
        {
            if (comPortLayout._serial.IsOpen)
            {
                comPortLayout._serial.Close();
                if (!comPortLayout._serial.IsOpen)
                {
                    comPortLayout._buttonClose.Enabled = false;
                    comPortLayout._buttonOpen.Enabled = true;
                    debugDataPrintLn("Close Port");
                }
            }
        }
        public void debugDataPrint(string str)
        {
            SetText(comPortLayout._form, comPortLayout._serialOut, str);
        }
        public void debugDataPrintLn(string str)
        {
            SetText(comPortLayout._form, comPortLayout._serialOut, str + "\r\n");
        }
        public void serialSendCommand(byte[] data, int length)
        {
            comPortLayout._serial.Write(data, 0, length);
            debugDataPrintLn(">> " + BitConverter.ToString(data).Replace("-", " ") + "\n\r");
        }
    }
    class serial
    {
        private byte[] StartByte = { 0x4C, 0x4D };

        private static System.Timers.Timer SendDataTimer;
        public byte globalSeqNumber = 0;

        public comPort comPort = new comPort();
        private List<Cmd_t> list_cmd = new List<Cmd_t>();

        public struct Cmd_t
        {
            public Int32 cmdId;
            public byte length;
            public byte seqNumber;
            public byte[] cmdData;
            public byte checkXor()
            {
                byte cxor = 0;
                for (int i = 0; i < this.length - 2; i++)
                {
                    cxor ^= this.cmdData[i];
                }
                return cxor;
            }
        };


        public void Init(comPort.ComPortLayout comPortLayout)
        {
            comPort.Init(comPortLayout);
            // start timer, cho phep truyen moi ban tin do tre 500ms
            SendDataTimer = new System.Timers.Timer(200);
            // Hook up the Elapsed event for the timer. 
            SendDataTimer.Elapsed += OnTimedEvent;
            SendDataTimer.AutoReset = true;
            SendDataTimer.Enabled = true;
        }

        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            if (list_cmd.Count > 0)
            {
                Cmd_t cmd = list_cmd.First();
                list_cmd.Remove(cmd);
                cmd.seqNumber = globalSeqNumber++;
                int index = 0;
                byte[] data = new byte[cmd.length + 3];
                data[index++] = (byte)StartByte[0];
                data[index++] = (byte)StartByte[1];
                data[index++] = (cmd.length);
                data[index++] = (cmd.seqNumber);
                for (int i = 0; i < cmd.length - 2; i++)
                {
                    data[index++] = cmd.cmdData[i];
                }
                data[index++] = cmd.checkXor();
                comPort.serialSendCommand(data, data.Length);
            }
        }

        public void SendCmd(Cmd_t cmd)
        {
            list_cmd.Add(cmd);
        }
        public byte[] StructureToByteArray(object obj)
        {
            int len = Marshal.SizeOf(obj);
            byte[] arr = new byte[len];
            IntPtr ptr = Marshal.AllocHGlobal(len);
            Marshal.StructureToPtr(obj, ptr, true);
            Marshal.Copy(ptr, arr, 0, len);
            Marshal.FreeHGlobal(ptr);
            return arr;
        }

    }
}
